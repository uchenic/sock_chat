import socket
import _thread
thread = _thread
import sys
import time

def operate(main_port):
    listen_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    listen_s.bind((HOST, main_port))
    listen_s.listen(1)
    users=[]
    while len(users)<2:

        s, addr = listen_s.accept()
        users.append(s)
    listen_s.close()
#    print('Socket on port %d connected by %s' % (main_port, addr))
    thread.start_new_thread(reader, (users[0],users[1]))
    reader(users[1],users[0])

def reader(s,r):
    ch = s.recv(1)
    r.send(ch)
    while ch:
        sys.stdout.buffer.write(ch)
        sys.stdout.flush()
        ch = s.recv(1)
        r.send(ch)
    s.close()
    r.close()

#def writer(s):
#    ch = sys.stdin.buffer.read(1)
#    while ch:
#        s.send(ch)
#        ch = sys.stdin.buffer.read(1)

HOST = ''
if len(sys.argv) < 2:
    print("Usage: port")
else:
    main_port = int(sys.argv[1])
    operate(main_port)
